//
//  UpcomingLaunchesViewModelTest.swift
//  UpcomingLaunchesTests
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import XCTest
@testable import MavFarm

class UpcomingLaunchesViewModelTest: XCTestCase {

    func test01_fetchMissions() {
        let mockAPI = APIClientMock()
        var missions: [Mission] = []
        for _ in 0..<10 {
            let mission = Mission.getMockData()
            missions.append(mission)
        }
        mockAPI.mockReturns[missionsURL] = missions
        let viewModel = UpcomingLaunchesViewModel(apiClient: mockAPI)
        let exp = expectation(description: "fetch mission completed")
        viewModel.didUpdateMissionData = {
            exp.fulfill()
        }
        viewModel.fetchMissions()
        wait(for: [exp], timeout: 0.1)
        XCTAssertEqual(viewModel.missions.count, missions.count)
        XCTAssertEqual(mockAPI.lastFetchedURL, missionsURL)
        for i in 0..<missions.count {
            XCTAssertEqual(viewModel.missions[i].name, missions[i].name)
            XCTAssertEqual(viewModel.missions[i].time, missions[i].time)
            XCTAssertEqual(viewModel.missions[i].ids, missions[i].ids)
        }
    }
    
    func test02_fetchMissionError() {
        let mockAPI = APIClientMock()
        mockAPI.mockErorrs[missionsURL] = NSError(domain: "test", code: 500, userInfo: nil)
        let viewModel = UpcomingLaunchesViewModel(apiClient: mockAPI)
        let exp = expectation(description: "fetch mission failed")
        viewModel.errorOccurred = { (_) in
            exp.fulfill()
        }
        viewModel.fetchMissions()
        wait(for: [exp], timeout: 0.1)
        XCTAssertEqual(mockAPI.lastFetchedURL, missionsURL)
    }
    
    func test03_fetchNextMission() {
        let mockAPI = APIClientMock()
        let mission = Mission.getMockData()
        mockAPI.mockReturns[nextMissionsURL] = mission
        let viewModel = UpcomingLaunchesViewModel(apiClient: mockAPI)
        let exp = expectation(description: "fetch mission completed")
        viewModel.didUpdateNextMission = {
            exp.fulfill()
        }
        viewModel.fetchNextMission()
        wait(for: [exp], timeout: 0.1)
        XCTAssertEqual(viewModel.nextMission?.name, mission.name)
        XCTAssertEqual(viewModel.nextMission?.time, mission.time)
        XCTAssertEqual(viewModel.nextMission?.ids, mission.ids)
        XCTAssertEqual(mockAPI.lastFetchedURL, nextMissionsURL)
    }
    
    func test04_fetchNextMissionError() {
        let mockAPI = APIClientMock()
        mockAPI.mockErorrs[nextMissionsURL] = NSError(domain: "test", code: 500, userInfo: nil)
        let viewModel = UpcomingLaunchesViewModel(apiClient: mockAPI)
        let exp = expectation(description: "fetch mission failed")
        viewModel.errorOccurred = { (_) in
            exp.fulfill()
        }
        viewModel.fetchNextMission()
        wait(for: [exp], timeout: 0.1)
        XCTAssertEqual(mockAPI.lastFetchedURL, nextMissionsURL)
    }
}
