//
//  MainViewControllerTest.swift
//  UpcomingLaunchesTests
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import XCTest
@testable import MavFarm

class MainViewControllerTest: XCTestCase {

    func test01_viewModelBinding() {
        let apiMock = APIClientMock()
        apiMock.mockReturns[missionsURL] = [Mission.getMockData(), Mission.getMockData()]
        apiMock.mockReturns[nextMissionsURL] = Mission.getMockData()
        let viewModel = UpcomingLaunchesViewModel(apiClient: apiMock)
        let viewController = MainViewController()
        viewController.viewModel = viewModel
        let exp = expectation(for: NSPredicate(format: "isViewLoaded == true"), evaluatedWith: viewController, handler: nil)
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.rootViewController = viewController
        window.makeKeyAndVisible()
        wait(for: [exp], timeout: 2.0)
        guard let tableView = viewController.missionTableView else {
            XCTFail("tableView not created")
            return
        }
        XCTAssertEqual(viewController.numberOfSections(in: tableView), viewModel.sections.count)
        XCTAssertEqual(viewController.tableView(tableView, numberOfRowsInSection: viewModel.sections[0].rawValue), 1)
        XCTAssertEqual(viewController.tableView(tableView, numberOfRowsInSection: viewModel.sections[1].rawValue), 2)
    }
}
