//
//  SampleData.swift
//  UpcomingLaunchesTests
//
//  Created by Chris on 8/10/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation

let sampleLaunchData = """
[
    {
        "flight_number": 84,
        "mission_name": "Starlink 2",
        "mission_id": ["123"],
        "launch_year": "2019",
        "launch_date_unix": 1571270400,
        "launch_date_utc": "2019-10-17T00:00:00.000Z",
        "launch_date_local": "2019-10-16T20:00:00-04:00",
        "is_tentative": true,
        "tentative_max_precision": "day",
        "tbd": false,
        "launch_window": null,
        "rocket": {
            "rocket_id": "falcon9",
            "rocket_name": "Falcon 9",
            "rocket_type": "FT",
            "first_stage": {
                "cores": [
                {
                "core_serial": null,
                "flight": null,
                "block": 5,
                "gridfins": true,
                "legs": true,
                "reused": true,
                "land_success": null,
                "landing_intent": true,
                "landing_type": "ASDS",
                "landing_vehicle": "OCISLY"
                }
                ]
            },
            "second_stage": {
                "block": 5,
                "payloads": [
                {
                "payload_id": "Starlink 2",
                "norad_id": [],
                "reused": false,
                "customers": [
                "SpaceX"
                ],
                "nationality": "United States",
                "manufacturer": "SpaceX",
                "payload_type": "Satellite",
                "payload_mass_kg": null,
                "payload_mass_lbs": null,
                "orbit": "VLEO",
                "orbit_params": {
                "reference_system": "geocentric",
                "regime": "very-low-earth",
                "longitude": null,
                "semi_major_axis_km": null,
                "eccentricity": null,
                "periapsis_km": null,
                "apoapsis_km": null,
                "inclination_deg": null,
                "period_min": null,
                "lifespan_years": null,
                "epoch": null,
                "mean_motion": null,
                "raan": null,
                "arg_of_pericenter": null,
                "mean_anomaly": null
                }
                }
                ]
            },
            "fairings": {
                "reused": false,
                "recovery_attempt": false,
                "recovered": false,
                "ship": null
            }
        },
        "ships": [],
        "telemetry": {
            "flight_club": null
        },
        "launch_site": {
            "site_id": "ccafs_slc_40",
            "site_name": "CCAFS SLC 40",
            "site_name_long": "Cape Canaveral Air Force Station Space Launch Complex 40"
        },
        "launch_success": null,
        "links": {
            "mission_patch": null,
            "mission_patch_small": null,
            "reddit_campaign": null,
            "reddit_launch": null,
            "reddit_recovery": null,
            "reddit_media": null,
            "presskit": null,
            "article_link": null,
            "wikipedia": null,
            "video_link": null,
            "youtube_id": null,
            "flickr_images": []
        },
        "details": "This mission will launch the first batch of Starlink version 1.0 satellites, from SLC-40, Cape Canaveral AFS. It is the second Starlink launch overall. Starlink is a low Earth orbit broadband internet constellation developed and owned by SpaceX which will eventually consist of nearly 12 000 satellites and will provide low latency internet service to ground terminals around the world. The booster for this mission is expected to land on OCISLY.",
        "upcoming": true,
        "static_fire_date_utc": null,
        "static_fire_date_unix": null,
        "timeline": null,
        "crew": null
}]
"""
