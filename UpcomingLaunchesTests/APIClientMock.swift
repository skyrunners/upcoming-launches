//
//  APIClientMock.swift
//  UpcomingLaunchesTests
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation
@testable import MavFarm


class APIClientMock: APIClientProtocol {
    
    var mockReturns: [URL: Any] = [:]
    var mockErorrs: [URL: Error] = [:]
    var lastFetchedURL: URL?
    
    func fetchData<T>(endpoint: URL, completion: APIResponse<T>?) {
        lastFetchedURL = endpoint
        if let error = mockErorrs[endpoint] {
            completion?(nil, error)
            return
        }
        completion?(mockReturns[endpoint] as? T, nil)
    }
}
