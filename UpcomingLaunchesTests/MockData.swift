//
//  MockData.swift
//  UpcomingLaunchesTests
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation
@testable import MavFarm

extension Mission {
    static func getMockData(missionName: String? = nil,
                            missionTime: Date? = nil,
                            missionId: String? = nil,
                            rocketName: String? = nil,
                            firstStageReused: Bool = false,
                            secondStageReused: Bool = false,
                            fairingReused: Bool = false) -> Mission {
        let mName = missionName ?? UUID().uuidString
        let mTime = missionTime ?? Date()
        var mId: [String] = []
        if let missionId = missionId {
            mId.append(missionId)
        }
        let rName = rocketName ?? UUID().uuidString
        let firstStage = Rocket.FirstStage(cores: [Rocket.FirstStage.Core(reused: firstStageReused)])
        let secondStage = Rocket.SecondStage(payloads: [Rocket.SecondStage.Payload(reused: secondStageReused)])
        let fairing = Rocket.Fairings(reused: fairingReused)
        let rocket = Rocket(name: rName, firstStage: firstStage, secondStage: secondStage, fairings: fairing)
        let mission = Mission(name: mName, time: mTime, ids: mId, rocket: rocket)
        return mission        
    }
}
