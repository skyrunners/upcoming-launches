//
//  DataParsing.swift
//  UpcomingLaunchesTests
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import XCTest
@testable import MavFarm

class DataParsing: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test01_parseLaunch() {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = DataDateDecodingFormat
        guard let data = sampleLaunchData.data(using: .utf8) else {
            XCTFail("unable to get sample data")
            return
        }
        let mission: Mission?
        do {
            mission = try decoder.decode([Mission].self, from: data).first
            XCTAssertNotNil(mission)
            XCTAssertEqual(mission?.name, "Starlink 2")
            XCTAssertEqual(mission?.time, Date(timeIntervalSince1970: 1571270400))
            XCTAssertEqual(mission?.ids.first, "123")
            XCTAssertEqual(mission?.rocket.name, "Falcon 9")
            XCTAssertTrue(mission?.rocket.firstStage.cores.first?.reused ?? false)
            XCTAssertFalse(mission?.rocket.secondStage.payloads.first?.reused ?? true)
            XCTAssertFalse(mission?.rocket.fairings?.reused ?? true)
        } catch let error {
            XCTFail("Unable to parse data \(error)")
        }
    }
}
