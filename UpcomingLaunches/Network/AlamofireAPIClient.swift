//
//  AlamofireAPIClient.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireAPIClient: APIClientProtocol {
    private let alamofireSessionManager: SessionManager
    
    init() {
        alamofireSessionManager = SessionManager.default
    }
    
    func fetchData<T: Codable>(endpoint: URL, completion: ((T?, Error?) -> Void)?) {
        alamofireSessionManager.request(endpoint).responseData { (response) in
            if let error = response.error {
                completion?(nil, error)
                return
            }
            if let data = response.value {
                let jsonDecoder = JSONDecoder()
                jsonDecoder.dateDecodingStrategy = DataDateDecodingFormat
                var responseObj: T?
                do {
                    responseObj = try jsonDecoder.decode(T.self, from: data)
                } catch let error {
                    completion?(nil, error)
                    return
                }
                completion?(responseObj, nil)                
            }
        }
    }
}
