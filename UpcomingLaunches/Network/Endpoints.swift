//
//  Endpoints.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation
let baseURL: String = "https://api.spacexdata.com/v3"
let missionsURL: URL = URL(string: "\(baseURL)/launches/upcoming")!
let nextMissionsURL: URL = URL(string: "\(baseURL)/launches/next")!
