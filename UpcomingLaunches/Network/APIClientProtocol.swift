//
//  APIClientProtocol.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation

typealias APIResponse<T> = ((T?, Error?) -> Void)

protocol APIClientProtocol {
    func fetchData<T: Codable>(endpoint: URL, completion: APIResponse<T>?)
}

let DataDateDecodingFormat: JSONDecoder.DateDecodingStrategy = .secondsSince1970
