//
//  ViewController.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/10/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import UIKit
import PureLayout

class MainViewController: UIViewController {

    var viewModel: UpcomingLaunchesViewModel?
    private (set) var missionTableView: UITableView?
    private var activityIndicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //UI Setup
        view.backgroundColor = .white
        let tableView = UITableView(frame: CGRect.zero, style: .plain)
        tableView.register(MissionTableViewCell.self, forCellReuseIdentifier: String(describing: MissionTableViewCell.self))
        tableView.register(NextMissionTableViewCell.self, forCellReuseIdentifier: String(describing: NextMissionTableViewCell.self))
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 38
        view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdges()
        tableView.dataSource = self
        tableView.allowsSelection = false
        self.missionTableView = tableView
        
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
        activityIndicator.autoCenterInSuperview()
        activityIndicator.startAnimating()
        self.activityIndicator = activityIndicator
        
        bindViewModel()
    }

    func bindViewModel() {
        guard let viewModel = viewModel else {
            return
        }
        viewModel.didUpdateMissionData = { [weak self] in
            DispatchQueue.main.async {
                let section = UpcomingLaunchesViewModel.Section.upcomingMissions.rawValue
                self?.missionTableView?.reloadSections(IndexSet(integer: section), with: .automatic)
                self?.activityIndicator?.stopAnimating()
            }
        }
        viewModel.didUpdateNextMission = { [weak self] in
            DispatchQueue.main.async {
                let section = UpcomingLaunchesViewModel.Section.nextMission.rawValue
                self?.missionTableView?.reloadSections(IndexSet(integer: section), with: .automatic)
            }
        }
        viewModel.errorOccurred = { [weak self] (error) in
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: nil))
            self?.present(alert, animated: true, completion: nil)
        }
        viewModel.fetchMissions()
        viewModel.fetchNextMission()
    }
}

extension MainViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel?.sections.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfItemsIn(section: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = viewModel else {
            return UITableViewCell(forAutoLayout: ())
        }
        let section = viewModel.sections[indexPath.section]
        switch section {
        case .nextMission:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: NextMissionTableViewCell.self), for: indexPath)
            guard let missionCell = cell as? NextMissionTableViewCell else {
                return cell
            }
            missionCell.mission = viewModel.nextMission
            return missionCell
        case .upcomingMissions:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MissionTableViewCell.self), for: indexPath)
            guard let missionCell = cell as? MissionTableViewCell else {
                return cell
            }
            missionCell.mission = viewModel.missions[indexPath.row]
            return missionCell
        }
        
    }
}

