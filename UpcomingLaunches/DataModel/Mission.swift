//
//  Mission.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/10/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation

struct Mission: Codable {
    let name: String
    let time: Date
    let ids: [String]
    let rocket: Rocket
    
    enum CodingKeys: String, CodingKey {
        case name = "mission_name"
        case time = "launch_date_unix"
        case ids = "mission_id"
        case rocket
    }
    
}
