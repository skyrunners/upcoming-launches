//
//  Rocket.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/10/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation

struct Rocket: Codable {
    let name: String
    let firstStage: FirstStage
    let secondStage: SecondStage
    let fairings: Fairings?
    
    enum CodingKeys: String, CodingKey {
        case name = "rocket_name"
        case firstStage = "first_stage"
        case secondStage = "second_stage"
        case fairings = "fairings"
    }
    
    struct FirstStage: Codable {
        let cores: [Core]
        
        enum CodingKeys: String, CodingKey {
            case cores
        }
        
        struct Core: Codable {
            let reused: Bool?
            
            enum CodingKeys: String, CodingKey {
                case reused
            }
        }
    }
    
    struct SecondStage: Codable {
        let payloads: [Payload]
        
        enum CodingKeys: String, CodingKey {
            case payloads
        }
        
        struct Payload: Codable {
            let reused: Bool?
            
            enum CodingKeys: String, CodingKey {
                case reused
            }
        }
    }
    
    struct Fairings: Codable {
        let reused: Bool?
        
        enum CodingKeys: String, CodingKey {
            case reused
        }
    }
}
