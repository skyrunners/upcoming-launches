//
//  Extensions.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
        return dateFormatter.string(from: self)
    }
}

extension TimeInterval {
    func toCountdownString() -> String {
        var str = ""
        let days = Int(self / (24 * 60 * 60))
        if days > 0 {
            str += String(format:"%02dD, ", days)
        }
        let hours = Int((self - Double(days) * 24 * 60 * 60) / (60 * 60))
        if hours > 0 || days > 0 {
            str += String(format:"%02dH, ", hours)
        }
        let minutes = Int((self - Double(days) * 24 * 60 * 60 - Double(hours) * 60 * 60)/60)
        if minutes > 0 || hours > 0 {
            str += String(format:"%02dM, ", minutes)
        }
        let seconds = Int(self - Double(days) * 24 * 60 * 60 - Double(hours) * 60 * 60 - Double(minutes) * 60)
        str += String(format:"%02dS", seconds)
        return str
    }
}


extension UIColor {
    static var themeGray: UIColor {
        return UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
    }
    
    static var themeDarkGray: UIColor {
        return UIColor(red:0.44, green:0.49, blue:0.55, alpha:1.0)
    }
    
    static var themeDarkText: UIColor {
        return UIColor(red:0.18, green:0.20, blue:0.23, alpha:1.0)
    }
    
    static var themeLightBrown: UIColor {
        return UIColor(red:0.74, green:0.71, blue:0.61, alpha:1.0)
    }
    
    static var themeLightGreen: UIColor {
        return UIColor(red:0.50, green:0.81, blue:0.73, alpha:1.0)
    }
    
    static var themeTeal: UIColor {
        return UIColor(red:0.25, green:0.67, blue:0.77, alpha:1.0)
    }
}
