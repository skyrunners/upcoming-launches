//
//  UpcomingLaunchesViewModel.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import Foundation

class UpcomingLaunchesViewModel {
    enum Section: Int {
        case nextMission, upcomingMissions
    }
    private let apiClient: APIClientProtocol
    private (set) var missions: [Mission] = []
    private (set) var nextMission: Mission?
    var didUpdateMissionData: (() -> Void)?
    var didUpdateNextMission: (() -> Void)?
    var errorOccurred: ((Error) -> Void)?
    let sections: [Section] = [.nextMission, .upcomingMissions]
    
    init(apiClient: APIClientProtocol) {
        self.apiClient = apiClient
    }
    
    //Note - pagination fetch omittted for simplicity
    func fetchMissions() {
        apiClient.fetchData(endpoint: missionsURL) { [weak self] (response: [Mission]?, error) in
            if let error = error {
                self?.errorOccurred?(error)
                return
            }
            self?.missions = response ?? []
            self?.didUpdateMissionData?()
        }
    }
    
    func fetchNextMission() {
        apiClient.fetchData(endpoint: nextMissionsURL) { [weak self] (response: Mission?, error) in
            if let error = error {
                self?.errorOccurred?(error)
                return
            }
            self?.nextMission = response
            self?.didUpdateNextMission?()
        }
    }
    
    func numberOfItemsIn(section: Int) -> Int {
        guard section < sections.count else {
            return 0
        }
        let sectionType = sections[section]
        switch sectionType {
        case .nextMission:
            return nextMission == nil ? 0 : 1
        case .upcomingMissions:
            return missions.count
        }
    }
}
