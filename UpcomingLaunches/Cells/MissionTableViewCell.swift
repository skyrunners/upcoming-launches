//
//  MissionTableViewCell.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import UIKit

class MissionTableViewCell: UITableViewCell {

    private var labelName: UILabel?
    private var labelTime: UILabel?
    private var labelRocketName: UILabel?
    private var labelFirstStage: UILabel?
    private var labelSecondStage: UILabel?
    private var labelFairing: UILabel?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let stackView = UIStackView(forAutoLayout: ())
        contentView.addSubview(stackView)
        stackView.distribution = .fillProportionally
        stackView.axis = .vertical
        stackView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .bottom)
        
        let labelTime = UILabel(forAutoLayout: ())
        labelTime.backgroundColor = UIColor.themeDarkText
        labelTime.textColor = UIColor.white
        self.labelTime = labelTime
        stackView.addArrangedSubview(labelTime)
        
        let labelName = UILabel(forAutoLayout: ())
        labelName.textColor = UIColor.themeDarkText
        labelName.minimumScaleFactor = 0.5
        labelName.adjustsFontSizeToFitWidth = true
        labelName.textAlignment = .center
        labelName.font = UIFont(name: labelName.font.fontName, size: 24.0)
        labelName.backgroundColor = UIColor.themeTeal
        self.labelName = labelName
        stackView.addArrangedSubview(labelName)
        
        let labelRocketName = UILabel(forAutoLayout: ())
        labelRocketName.font = UIFont(name: labelName.font.fontName, size: 24.0)
        labelRocketName.textColor = UIColor.themeDarkText
        labelRocketName.textAlignment = .center
        labelRocketName.backgroundColor = UIColor.themeLightGreen
        stackView.addArrangedSubview(labelRocketName)
        self.labelRocketName = labelRocketName
        
        let labelFirstStage = UILabel(forAutoLayout: ())
        labelFirstStage.text = "First Stage"
        labelFirstStage.textAlignment = .right
        stackView.addArrangedSubview(labelFirstStage)
        self.labelFirstStage = labelFirstStage
        
        let labelSecondStage = UILabel(forAutoLayout: ())
        labelSecondStage.text = "Second Stage"
        labelSecondStage.textAlignment = .right
        stackView.addArrangedSubview(labelSecondStage)
        self.labelSecondStage = labelSecondStage
        
        let labelFairing = UILabel(forAutoLayout: ())
        labelFairing.text = "Fairing"
        labelFairing.textAlignment = .right
        stackView.addArrangedSubview(labelFairing)
        self.labelFairing = labelFairing
        
        let viewSpacer = UIView(forAutoLayout: ())
        viewSpacer.autoSetDimension(.height, toSize: 20)
        viewSpacer.backgroundColor = UIColor.themeGray
        contentView.addSubview(viewSpacer)
        viewSpacer.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        viewSpacer.autoPinEdge(.top, to: .bottom, of: stackView)
    }
    
    //Note - for simplicity, data binding happens directly with Mission data model instead of a separate MissionCellViewModel
    var mission: Mission? {
        didSet {
            bindData()
        }
    }
    
    func bindData() {
        guard let mission = mission else {
            return
        }
        var name: [String] = [mission.name]
        if let id = mission.ids.first {
            name.insert(id, at: 0)
        }
        labelName?.text = "👨🏻‍🚀 \(name.joined(separator: " | "))"
        labelTime?.text = mission.time.toString()
        labelRocketName?.text = "🚀 \(mission.rocket.name)"
        labelFirstStage?.text = "First Stage Reuse: " + (mission.rocket.firstStage.cores.first?.reused == true ? "♻" : "🗑️")
        labelSecondStage?.text = "Second Stage Reuse: " + (mission.rocket.secondStage.payloads.first?.reused == true  ? "♻" : "🗑️")
        labelFairing?.text = "Fairing Reuse: " + (mission.rocket.fairings?.reused == true  ? "♻" : "🗑️")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        labelFirstStage?.backgroundColor = .white
        labelSecondStage?.backgroundColor = .white
        labelFirstStage?.backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
