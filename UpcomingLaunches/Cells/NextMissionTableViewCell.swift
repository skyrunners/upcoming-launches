//
//  NextMissionTableViewCell.swift
//  UpcomingLaunches
//
//  Created by Chris on 8/11/19.
//  Copyright © 2019 Mav Farm. All rights reserved.
//

import UIKit

class NextMissionTableViewCell: UITableViewCell {

    private var labelTimer: UILabel?
    private var timer: Timer?
    
    var mission: Mission? {
        didSet {
            bindDataModel()
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let labelTitle = UILabel(forAutoLayout: ())
        labelTitle.font = UIFont(name: labelTitle.font.fontName, size: 20.0)
        labelTitle.text = "Time till next launch"
        contentView.addSubview(labelTitle)
        labelTitle.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .bottom)
        labelTitle.backgroundColor = UIColor.themeGray
        labelTitle.textColor = UIColor.themeDarkText
        let labelTimer = UILabel(forAutoLayout: ())
        contentView.addSubview(labelTimer)
        labelTimer.font = UIFont.monospacedDigitSystemFont(ofSize: 26.0, weight: .semibold)
        labelTimer.textAlignment = .center
        labelTimer.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        labelTimer.autoPinEdge(.top, to: .bottom, of: labelTitle)
        labelTimer.backgroundColor = UIColor.themeLightBrown
        labelTimer.textColor = UIColor.themeDarkText
        self.labelTimer = labelTimer
    }
    
    func bindDataModel() {
        guard let mission = mission else {
            timer?.invalidate()
            return
        }
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] (_) in
            self?.setCountDownText(mission.time)
        })
        setCountDownText(mission.time)
    }
    
    private func setCountDownText(_ date: Date) {
        labelTimer?.text = "\(date.timeIntervalSinceNow.toCountdownString())"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        timer?.invalidate()
        timer = nil
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
