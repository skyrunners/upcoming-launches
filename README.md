# upcoming-launches

Compile Instruction:

1. install cocoapods if haven't done so already. (sudo gem install cocoapods)
2. In the project directory, run pod update
3. Open UpcomingLaunches.xcworkspace
4. To launch, press cmd + r
5. To run unit tests, press cmd + u

Notes - 
1. The project uses MVVM architecture, with APIClient (the only dependency) initialized in AppDelegate.
2. For simplicity, data model has been bound directly into each table cell.  A propery way to do this would be creating a table view cell view model.
3. For simplicity, pagination for fetching upcoming launches is ommitted.

